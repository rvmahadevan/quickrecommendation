FROM python:3.7
MAINTAINER Mahadevan Varadhan


# Create app directory
RUN mkdir ~/.aws

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY app/. .
COPY requirements.txt .

# Install app dependencies

RUN pip install --upgrade pip

RUN pip install pandas && \
    pip install nltk && \
    pip install sqlalchemy && \
    pip install psycopg2-binary && \
    pip install pyarrow && \
    pip install spacy  && \
    pip install pyyaml && \
    pip install argparse && \
    pip install boto3 && \
    pip install awscli
    
RUN pip3 install -U scikit-learn

RUN [ "python", "-c", "import nltk; nltk.download('all')" ]
    
RUN pip install --upgrade --no-cache-dir -r requirements.txt

EXPOSE 5000 8080 7000


