import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import boto3
import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import pairwise_distances
import sys, string
from sys import argv
from utils.s3_utils import list_s3_objects, read_ss3_objects, read_s3_multiobjects


class LoadUserBasedData:
    def __init__(self, bucket, keypath, userfilepath, itemfilepath):
        self.bucket = bucket
        self.keypath = keypath
        self.userfilepath = userfilepath
        self.itemfilepath = itemfilepath
    
    def load_users(self):
        user_df = read_ss3_objects(self.bucket, self.userfilepath)
        return user_df
    
    def load_items(self):
        item_df = read_ss3_objects(self.bucket, self.itemfilepath)
        return item_df
      
    def load_iterations(self):
        activity_df = read_s3_multiobjects(self.bucket, self.keypath)
        return activity_df
    
class TrainS3RecomendationData:
    def __init__(self, bucket, keypath, userfilepath, itemfilepath, datapath):
        self.bucket = bucket
        self.keypath = keypath
        self.userfilepath = userfilepath
        self.itemfilepath = itemfilepath
        self.datapath = datapath
               
    def find_n_neighbours(self, df, n):
        order = np.argsort(df.values, axis=1)[:, :n]
        df = df.apply(lambda x: pd.Series(x.sort_values(ascending=False)
               .iloc[:n].index, 
              index=['top{}'.format(i) for i in range(1, n+1)]), axis=1)
        return df
      
    def prepare_rec_data(self):
        ### STEP 1: Extract Data from S3 Bucket
        user_ds = read_ss3_objects(self.bucket, self.userfilepath)
        item_ds = read_ss3_objects(self.bucket, self.itemfilepath)
        activity_ds = read_s3_multiobjects(self.bucket, self.keypath)
        ### STEP 2: Prepare and Clean Data
        activity_ds.columns = ['USER_ID','ITEM_ID','SCORE_TYPE', 'SCORE', 'TIMESTAMP']
        activity_item = pd.merge(activity_ds, item_ds, on = 'ITEM_ID', how = 'outer')
        tags = activity_item[['USER_ID', 'ITEM_ID', 'GENRE', 'TIMESTAMP']]
        mean_ds = activity_ds.groupby(by = "USER_ID",as_index = False)['SCORE'].mean()
        score_avg = pd.merge(activity_ds, mean_ds, on='USER_ID')
        score_avg['ADG_SCORE'] = score_avg['SCORE_x'] - score_avg['SCORE_y']
        check = pd.pivot_table(score_avg,values = 'SCORE_x', index = 'USER_ID', columns='ITEM_ID')
        final = pd.pivot_table(score_avg,values = 'ADG_SCORE', index = 'USER_ID', columns='ITEM_ID')
        # Replacing NaN by Content Average
        final_item = final.fillna(final.mean(axis=0))
        # Replacing NaN by user Average
        final_user = final.apply(lambda row: row.fillna(row.mean()), axis=1)
        ## Create User Simillarity Matrix by identifying users spent same time in simillar content
        user_cosine = cosine_similarity(final_user)
        np.fill_diagonal(user_cosine, 0)
        similarity_by_user = pd.DataFrame(user_cosine,index=final_user.index)
        similarity_by_user.columns=final_user.index
        ## User similarity on replacing NAN by item avg
        ## Create User Simillarity Matrix by identifying users read simillar content
        item_cosine = cosine_similarity(final_item)
        np.fill_diagonal(item_cosine, 0)
        similarity_by_item = pd.DataFrame(item_cosine,index=final_item.index)
        similarity_by_item.columns=final_user.index
        # Top 10 neighbours for each user using similarity_with_user dataframe (simillar Timespent or Rating/Likes given)
        similarity_by_user_30 = self.find_n_neighbours(similarity_by_user, 30)
        # Top 10 neighbours for each user using similarity_with_content dataframe (simillar content read by users)
        similarity_by_item_30 = self.find_n_neighbours(similarity_by_item, 30)
        score_avg = score_avg.astype({"ITEM_ID": str})
        item_user = score_avg.groupby(by = 'USER_ID')['ITEM_ID'].apply(lambda x:','.join(x))
        check.to_pickle(self.datapath+'/{}'.format('check.pkl'))
        item_ds.to_pickle(self.datapath+'/{}'.format('item.pkl'))
        item_user.to_pickle(self.datapath+'/{}'.format('item_user.pkl'))
        final_item.to_pickle(self.datapath+'/{}'.format('final_item.pkl'))
        mean_ds.to_pickle(self.datapath+'/{}'.format('mean_ds.pkl'))
        similarity_by_item.to_pickle(self.datapath+'/{}'.format('similarity_by_item.pkl'))
        similarity_by_item_30.to_pickle(self.datapath+'/{}'.format('similarity_by_item_30.pkl'))
        similarity_by_user.to_pickle(self.datapath+'/{}'.format('similarity_by_user.pkl'))
        similarity_by_user_30.to_pickle(self.datapath+'/{}'.format('similarity_by_user_30.pkl'))
        score_avg.to_pickle(self.datapath+'/{}'.format('score_avg.pkl'))
        #return similarity_by_item_30
        

    
class GetRecommendation():
    def __init__(self, userid, datapath):
        self.userid = userid
        self.datapath = datapath
              
    def recommend_by_similar_item(self):
        check = pd.read_pickle(self.datapath+'/{}'.format('check.pkl'))
        similarity_by_item_30 = pd.read_pickle(self.datapath+'/{}'.format('similarity_by_item_30.pkl'))
        item_user = pd.read_pickle(self.datapath+'/{}'.format('item_user.pkl'))
        final_item = pd.read_pickle(self.datapath+'/{}'.format('final_item.pkl'))
        mean_ds = pd.read_pickle(self.datapath+'/{}'.format('mean_ds.pkl'))
        item_ds = pd.read_pickle(self.datapath+'/{}'.format('item.pkl'))
        similarity_by_item = pd.read_pickle(self.datapath+'/{}'.format('similarity_by_item.pkl'))
        ##################################################################################
        item_read_by_user = check.columns[check[check.index==self.userid].notna().any()].tolist()
        a = similarity_by_item_30[similarity_by_item_30.index==self.userid].values
        b = a.squeeze().tolist()
        d = item_user[item_user.index.isin(b)]
        l = ','.join(d.values)
        item_read_by_similar_users = l.split(',')
        item_under_consideration = list(set(item_read_by_similar_users)-set(list(map(str, item_read_by_user))))
        item_under_consideration = list(map(int, item_under_consideration))
        score = []
        for item in item_under_consideration:
            c = final_item.loc[:,item]
            d = c[c.index.isin(b)]
            f = d[d.notnull()]
            avg_user = mean_ds.loc[mean_ds['USER_ID'] == self.userid,'SCORE'].values[0]
            index = f.index.values.squeeze().tolist()
            corr = similarity_by_item.loc[self.userid, index]
            fin = pd.concat([f, corr], axis=1)
            fin.columns = ['adg_score','correlation']
            fin['score']=fin.apply(lambda x:x['adg_score'] * x['correlation'],axis=1)
            nume = fin['score'].sum()
            deno = fin['correlation'].sum()
            final_score = avg_user + (nume/deno)
            score.append(final_score)
        data = pd.DataFrame({'ITEM_ID':item_under_consideration,'score':score})
        top_10_recommendation = data.sort_values(by='score',ascending=False).head(10)
        item_name = top_10_recommendation.merge(item_ds, how='inner', on='ITEM_ID')
        item_list = item_name.ITEM_ID.values.tolist()
        final_recommendation = list(dict.fromkeys(item_list))
        return final_recommendation
    
    def recommend_by_similar_user(self):
        check = pd.read_pickle(self.datapath+'/{}'.format('check.pkl'))
        similarity_by_user_30 = pd.read_pickle(self.datapath+'/{}'.format('similarity_by_user_30.pkl'))
        item_user = pd.read_pickle(self.datapath+'/{}'.format('item_user.pkl'))
        final_item = pd.read_pickle(self.datapath+'/{}'.format('final_item.pkl'))
        mean_ds = pd.read_pickle(self.datapath+'/{}'.format('mean_ds.pkl'))
        item_ds = pd.read_pickle(self.datapath+'/{}'.format('item.pkl'))
        similarity_by_user = pd.read_pickle(self.datapath+'/{}'.format('similarity_by_user.pkl'))
        ##################################################################################
        item_read_by_user = check.columns[check[check.index==self.userid].notna().any()].tolist()
        a = similarity_by_user_30[similarity_by_user_30.index==self.userid].values
        b = a.squeeze().tolist()
        d = item_user[item_user.index.isin(b)]
        l = ','.join(d.values)
        item_read_by_similar_users = l.split(',')
        item_under_consideration = list(set(item_read_by_similar_users)-set(list(map(str, item_read_by_user))))
        item_under_consideration = list(map(int, item_under_consideration))
        score = []
        for item in item_under_consideration:
            c = final_item.loc[:,item]
            d = c[c.index.isin(b)]
            f = d[d.notnull()]
            avg_user = mean_ds.loc[mean_ds['USER_ID'] == self.userid,'SCORE'].values[0]
            index = f.index.values.squeeze().tolist()
            corr = similarity_by_user.loc[self.userid, index]
            fin = pd.concat([f, corr], axis=1)
            fin.columns = ['adg_score','correlation']
            fin['score']=fin.apply(lambda x:x['adg_score'] * x['correlation'],axis=1)
            nume = fin['score'].sum()
            deno = fin['correlation'].sum()
            final_score = avg_user + (nume/deno)
            score.append(final_score)
        data = pd.DataFrame({'ITEM_ID':item_under_consideration,'score':score})
        top_10_recommendation = data.sort_values(by='score',ascending=False).head(10)
        item_name = top_10_recommendation.merge(item_ds, how='inner', on='ITEM_ID')
        item_list = item_name.ITEM_ID.values.tolist()
        final_recommendation = list(dict.fromkeys(item_list))
        return final_recommendation
       





