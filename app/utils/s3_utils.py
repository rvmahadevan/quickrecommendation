import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import boto3
import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import pairwise_distances
import sys, string
from sys import argv
from config import Config

PY_CONFIG = Config("config", "test")

profile = PY_CONFIG.get('S3_PARAMS')['profile']

#### Boto S3 Connectioncd 
config = boto3.client('config')
session = boto3.session.Session(profile_name=profile)

#### Pandas Functions to Read S3 Files

def list_s3_buckets():
    s3 = boto3.client('s3')
    response = s3.list_buckets()
    buckets = [bucket['Name'] for bucket in response['Buckets']]
    # Print out the bucket list
    print("Bucket List: %s" % buckets)


### Lists objects in a S3 Location

def list_s3_objects(bucket, keypath):
    s3 = boto3.resource('s3')
    bkt = s3.Bucket(bucket)
    obj = []
    for file in bkt.objects.filter(Prefix=keypath):
        if file.key.endswith(".csv"):
            obj.append(file.key)
    return obj
        

    ## Test: 
    ## list_s3_objects(mybucket, mypath)


### Read data from Single s3 file

def read_ss3_objects(bucket, filepath):
    s3conn = boto3.client('s3')
    s3obj = s3conn.get_object(Bucket=bucket, Key=filepath)
    s3df = pd.read_csv(s3obj['Body'])
    return s3df

    ## Test: 
    ## sdf = read_ss3_objects(mybucket, filepath)
    ## sdf.head(2)


### Read data from multiple s3 file

def read_s3_multiobjects(bucket, keypath):
    obj = list_s3_objects(bucket, keypath)
    final = []
    for file in obj:
        if file.endswith(".csv"):
            s3conn = boto3.client('s3')
            s3obj = s3conn.get_object(Bucket=bucket, Key=file)
            s3df = pd.read_csv(s3obj['Body'])
            final.append(s3df)
    frame = pd.concat(final, axis=0, ignore_index=True)
    return frame


    # Test:
    # mdf = read_s3_multiobjects(mybucket, mypath)
    # mdf.head(2)



