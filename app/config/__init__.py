import yaml
import os
import re

class Config():
   def __init__(self, name, environment):
       try:
           with open(os.path.dirname(os.path.realpath(__file__))+'/'+name+'_'+environment+'.yaml', 'r') as f:
               self.conf = yaml.full_load(f)
               # load the config params
               for k, v in self.conf.items():
                   for child_key, child_value in v.items():
                       #check for environment variables
                       if re.match("^%(.*)%$",str(child_value)) is not None:
                           self.conf[k][child_key] = os.getenv(str(child_value).replace("%",""))
       except FileNotFoundError:
           print('Error: can\'t find config file '+name+'_'+environment)
         
   def get(self, key):
       return self.conf[key]
   def getAll(self):
       return self.conf