import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import argparse
from operators.s3_operators import GetRecommendation
from config import Config;
import argparse
import re
import logging

PY_CONFIG = Config("config", "test")
datapath = PY_CONFIG.get('LOCAL_PARAMS')['datapath']


class Recommend():

    def __init__(self, rectype, userid):
        self.rectype = rectype
        self.userid = userid
               
    def user_based_recommend(self):
        try:
            data = GetRecommendation(self.userid, datapath)
            recommend = data.recommend_by_similar_user()
            return recommend
        except:
            return {"error": {
            "message": "Please enter active userid"
            }}

    def item_based_recommend(self):
        try:
            data = GetRecommendation(self.userid, datapath)
            recommend = data.recommend_by_similar_item()
            return recommend
        except:
            return {"error": {
            "message": "Please enter active userid"
            }}
    
    def recommend(self):
        if self.rectype==1:
            data = self.user_based_recommend()
            return data
        if self.rectype==2:
            data = self.item_based_recommend()
            return data

# if __name__ == "__main__":
#     userid = 476127
#     rectype = 2
#     R=Recommend(rectype, userid)
#     item = R.recommend()
#     print(item)



def main(argv):
    #print(argv)
    parser = argparse.ArgumentParser(description="Recommendation")
    parser.add_argument("-m", "--userbased", dest="rectype", help="user based model or item based model")
    parser.add_argument("-u", "--userid", dest="userid", help="provide user id")
    args = parser.parse_args()
    rec = Recommend(rectype=int(argv[1]), userid=int(argv[3]))
    if int(argv[1])==1 :
        items = rec.user_based_recommend()
        print(items)
    elif int(argv[1])==2 :
        items = rec.item_based_recommend()
        print(items)
    else:
        print('no recommendation')
   
if __name__ == "__main__":
   main(sys.argv[1:])
