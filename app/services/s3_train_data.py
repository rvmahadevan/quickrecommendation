import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from operators.s3_operators import TrainS3RecomendationData
from config import Config;


PY_CONFIG = Config("config", "test")

profile = PY_CONFIG.get('S3_PARAMS')['profile']
bucket = PY_CONFIG.get('S3_PARAMS')['bucket']
keypath = PY_CONFIG.get('S3_PARAMS')['keypath']
userfilepath = PY_CONFIG.get('S3_PARAMS')['userfilepath']
itemfilepath = PY_CONFIG.get('S3_PARAMS')['itemfilepath']
datapath = PY_CONFIG.get('LOCAL_PARAMS')['datapath']

def train_data():
    try:
        print("Training in Progress")
        myservice = TrainS3RecomendationData(bucket, keypath, userfilepath, itemfilepath,datapath)
        myservice.prepare_rec_data()
        print("Training Completed")
    except:
        print("Service Failed")

if __name__ == "__main__":
    train_data()